# [MoboStore API](https://mobo-store.herokuapp.com/)
[MoboStore API Link](https://mobo-store.herokuapp.com/)

## Sign in 
get a token

Endpoints ``/users/:sign-in``

method ``POST``

request body:

```json
{
    "username":"",
    "password":""
}
```

## Sign up
register and get a token

Endpoints ``/users/:sign-up``

method ``POST``

request body:

```json
{
    "username":"",
    "password":""
}
```

## Using Token:
- put your token into Authorization field in the request Headers as the following:

```
Authorization: "bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZjcwYTFhMDNiYmMzZDBjOTgxYWIzYjYiLCJpYXQiOjE2MDEyMTcwMTQsImV4cCI6MjQ2NTIxNzAxNH0.Y-jxb3DAGq5txysxh6nW14xmWnCIonWxxJ3W3p-BWhg"
```

## Brands list

Endpoint ```/brands```

```json
[
    {
        "mobiles": [
             "5ee0f53e1297901a78ec515e",
             "5ee0f53e1297901a78ec515f",
             "5ee0f53e1297901a78ec5160",
             "5ee0f53e1297901a78ec5161"
         ],
         "_id": "5ee0f4226619041428acdfea",
         "name": "Huawei",
         "numberOfMobiles": 327,
         "url": "huawei-phones-58.php",
         "createdAt": "2020-06-10T14:54:26.314Z",
         "updatedAt": "2020-06-10T14:59:10.494Z",
         "__v": 0
     }
]
```

## Brand detail

Endpoints ``/brands/:ID``

example ``/brands/5ee0f4226619041428acdfea``
```json
{
    "_id": "5ee0f4226619041428acdfea",
    "name": "Huawei",
    "numberOfMobiles": 327,
    "url": "huawei-phones-58.php",
    "createdAt": "2020-06-10T14:54:26.314Z",
    "updatedAt": "2020-06-10T14:59:10.494Z",
    "__v": 0,
    "newestMobiles": [
        {
            "price": 280,
            "_id": "5ee0f53d1297901a78ec5022",
            "name": "Enjoy Z 5G",
            "brand": "Huawei",
            "img": "https://fdn2.gsmarena.com/vv/bigpic/huawei-enjoy-z-5g.jpg",
            "url": "huawei_enjoy_z_5g-10252.php",
            "description": "Huawei Enjoy Z 5G Android smartphone. Announced May 2020. Features 6.5″ IPS LCD display, MediaTek MT6873V Dimensity 800 5G chipset, 4000 mAh battery, 128 GB storage, 8 GB RAM.",
            "img_url": "huawei_enjoy_z_5g-pictures-10252.php",
            "title": "Huawei Enjoy Z 5G"
        }
    ]
}
```

## Add new brand

Endpoints ``/brands``

method ``POST``

privilege ``admin``

## Edit brand

Endpoints ``/brands/:ID``

method ``PUT``

privilege ``admin``

## Delete brand

Endpoints ``/brands/:ID``

method ``DELETE``

privilege ``admin``

## Mobiles list
get mobiles list

endpoint ``/mobiles``

```json
[
    {
        "price": 250,
        "_id": "5ee0f4cf56569b255011780c",
        "name": "Galaxy A41",
        "brand": "Samsung",
        "img": "https://fdn2.gsmarena.com/vv/bigpic/samsung-galaxy-a41.jpg",
        "url": "samsung_galaxy_a41-10138.php",
        "description": "Samsung Galaxy A41 Android smartphone. Announced Mar 2020. Features 6.1″ Super AMOLED display, MT6768 Helio P65 chipset, 3500 mAh battery, 64 GB storage, 4 GB RAM.",
        "img_url": "samsung_galaxy_a41-pictures-10138.php",
        "title": "Samsung Galaxy A41"
    }
]
```

## Mobile detail
get mobile spec detail

endpoint ``mobiles/:ID``

 example ``mobiels/5ee0f4cf56569b255011780c``

```json
{
  "price": 250,
  "full": true,
  "_id": "5ee0f4cf56569b255011780c",
  "name": "Galaxy A41",
  "brand": "Samsung",
  "img": "https://fdn2.gsmarena.com/vv/bigpic/samsung-galaxy-a41.jpg",
  "url": "samsung_galaxy_a41-10138.php",
  "spec_detail": [
    {
      "category": "Network",
      "specs": [
        {
          "name": "Technology",
          "value": "GSM / HSPA / LTE"
        }
      ]
    }
  ],
  "quick_spec": {
      "display_size": "6.1\"",
      "display_res": "1080x2400 pixels",
      "camera_pixels": "48MP\n      ",
      "video_pixels": "1080p",
      "ram_size": "4GB RAM",
      "chipset": "MT6768 Helio P65",
      "battery_size": "3500mAh",
      "battery_type": "Li-Po"
  },
  "createdAt": "2020-06-10T14:57:19.659Z",
  "updatedAt": "2020-06-10T15:01:04.517Z",
  "__v": 0,
  "img_url": "samsung_galaxy_a41-pictures-10138.php",
  "title": "Samsung Galaxy A41",
  "isNew": false
}
```

## Add new mobile

Endpoints ``/mobiles``

method ``POST``

privilege ``admin``

## Edit mobile

Endpoints ``/mobiles/:ID``

method ``PUT``

privilege ``admin``

## Delete mobile

Endpoints ``/mobiles/:ID``

method ``DELETE``

privilege ``admin``
